CC=gcc
CFLAGS=-Wall -ansi -pedantic -g
LDFLAGS=
NAME=obj2b3
SRC=src
DST=bin
INSTLOC=/usr/local/bin

SOURCES=$(wildcard $(SRC)/*.c)
INCLUDES=$(wildcard $(SRC/*.h)
OBJECTS=$(SOURCES:.c=.o)

$(DST)/(NAME): $(DST) $(OBJECTS)
	$(CC) $(OBJECTS) -o $(DST)/$(NAME) $(LDFLAGS)

$(DST):
	mkdir -p $(DST)

$(SRC)/%.o: $(SRC)/%.c
	$(CC) $(CFLAGS) -o $@ -c $<

install:
	cp $(DST)/$(NAME) $(INSTLOC)/$(NAME)

uninstall:
	rm $(INSTLOC)/$(NAME)

clean:
	rm -f $(OBJECTS)

