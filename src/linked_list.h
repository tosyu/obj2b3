#ifndef _LINKED_LIST_H_
#define _LINKED_LIST_H_

typedef struct _Node {
    void *data;
    struct _Node *next;
} TNode;

typedef struct _List {
    TNode *head;
    TNode *tail;
    TNode *current;
    int length;
} TList;

TList *listCreate(void);
void listDestroy(TList *);
TNode *listAddData(TList *, void *);
TNode *listAdd(TList *, TNode *);
int listRemoveByData(TList *, void *,
		     int (*compare) (const void *, const void *));
int listRemove(TList *, TNode *,
	       int (*compare) (const TNode *, const TNode *));
int listHasNextNode(TList *);
TNode *listNextNode(TList *);
void listRewind(TList *);
void listPurge(TList *, int, int);

#endif
