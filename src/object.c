#include "stdlib.h"
#include "string.h"
#include "stdio.h"
#include "object.h"

Object *objectCreate()
{
    Object *object = malloc(sizeof(Object));

    object->vertexData = listCreate();
    object->uvData = listCreate();
    object->normalData = listCreate();
    object->faceData = listCreate();

    return object;
}

void objectDestroy(Object * object)
{
    free(object);
}

void objectPurge(Object * object, int _objectDestroy, int purgeListDeep)
{
    listPurge(object->vertexData, 1, purgeListDeep);
    listPurge(object->uvData, 1, purgeListDeep);
    listPurge(object->normalData, 1, purgeListDeep);
    listPurge(object->faceData, 1, purgeListDeep);

    if (_objectDestroy) {
	objectDestroy(object);
    }
}

Object *objectParse(Object * object, char *data)
{
    char *token, *value, *fvalue;
    int result;
    Vertex *v;
    Uv *vt;
    Normal *vn;
    Face *f;
    if ((token = strtok(data, " ")) != NULL) {
	value = strtok(NULL, "\0");
	if (strcmp(token, "v") == 0) {
	    v = malloc(sizeof(Vertex));
	    result = sscanf(value, "%f %f %f %f", &(v->x), &(v->y), &(v->z), &(v->w));
	    if (result <= 3) {
		if (result == 3) {
		    v->w = 1.0;
		}
		listAddData(object->vertexData, (void *) v);
	    }
	} else if (strcmp(token, "vt") == 0) {
	    vt = malloc(sizeof(Uv));
	    result = sscanf(value, "%f %f %f", &(vt->u), &(vt->v), &(vt->w));
	    if (result <= 2) {
		if (result == 2) {
		    vt->w = 1.0;
		}
		listAddData(object->uvData, (void *) vt);
	    }
	} else if (strcmp(token, "vn") == 0) {
	    vn = malloc(sizeof(Normal));
	    result = sscanf(value, "%f %f %f", &(vn->x), &(vn->y), &(vn->z));
	    if (result == 3) {
		listAddData(object->normalData, (void *) vn);
	    }
	} else if (strcmp(token, "f") == 0) {
	    fvalue = strtok(value, " "); 
	    if (fvalue != NULL) {
		do {
		    f = malloc(sizeof(Face));
		    result = sscanf(value, "%d/%d/%d", &(f->v), &(f->vt), &(f->vn));
		    if (result > 1) {
			if (result == 2) {
			    if (f->vt == 0 && f->vn != 0) {
				f->vt = -1;
			    } else if (f->vn == 0 && f->vt != 0) {
				f->vn = -1;
			    }
			}
			listAddData(object->faceData, (void *) f);
		    }
		} while ((fvalue = strtok(NULL, " ")) != NULL);
	    }
	}
    }
    return object;
}
