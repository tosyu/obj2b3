#include "stdlib.h"
#include "stdio.h"
#include "b3.h"

B3 *b3Create()
{
	B3 *b3 = malloc(sizeof(B3));

	b3->header = malloc(sizeof(B3Header));
	b3->data = malloc(sizeof(B3Data));
	b3->indexes = malloc(sizeof(B3Indexes));

	b3->data->vertex = NULL;
	b3->data->uv = NULL;
	b3->data->normal = NULL;
	b3->indexes->vertex = NULL;
	b3->indexes->uv = NULL;
	b3->indexes->normal = NULL;

	return b3;
}

B3 *b3CreateFromObj(Object *object)
{
	B3 *b3 = b3Create();
	int i,
		vertexIndexesEnable = 0,
		uvIndexesEnable = 0,
		normalIndexesEnable = 0,
		index,
		vertexDataSize = sizeof(float) * object->vertexData->length * 4,
		uvDataSize = sizeof(float) * object->uvData->length * 3,
		normalDataSize = sizeof(float) * object->normalData->length * 3,
		indexDataSize = sizeof(int) * object->faceData->length;
	TNode *node;
	
	B3Header *header = b3->header;
	B3Data *data = b3->data;
	B3Indexes *indexes = b3->indexes;

	data->vertex = malloc(vertexDataSize);
	data->uv = malloc(uvDataSize);
	data->normal = malloc(normalDataSize);
	indexes->vertex = malloc(indexDataSize);
	indexes->uv = malloc(indexDataSize);
	indexes->normal = malloc(indexDataSize);

	i = 0;
	while(listHasNextNode(object->vertexData)) {
		node = listNextNode(object->vertexData);
		*((data->vertex) + i) = ((Vertex *) node->data)->x;
		*((data->vertex) + i + 1) = ((Vertex *) node->data)->y;
		*((data->vertex) + i + 2) = ((Vertex *) node->data)->z;
		*((data->vertex) + i + 3) = ((Vertex *) node->data)->w;
		i += 4;
	}
	listRewind(object->vertexData);

	i = 0;
	while(listHasNextNode(object->uvData)) {
		node = listNextNode(object->uvData);
		*((data->uv) + i) = ((Uv *) node->data)->u;
		*((data->uv) + i + 1) = ((Uv *) node->data)->v;
		*((data->uv) + i + 2) = ((Uv *) node->data)->w;
		i += 3;
	}
	listRewind(object->uvData);
	
	i = 0;
	while(listHasNextNode(object->normalData)) {
		node = listNextNode(object->normalData);
		*((data->normal) + i) = ((Normal *) node->data)->x;
		*((data->normal) + i + 1) = ((Normal *) node->data)->y;
		*((data->normal) + i + 2) = ((Normal *) node->data)->z;
		i += 3;
	}
	listRewind(object->normalData);
	
	i = 0;
	while(listHasNextNode(object->faceData)) {
		node = listNextNode(object->faceData);
		index = ((Face *) node->data)->v;
		if (index > -1) {
			*((indexes->vertex) + i) = index;
			if (!vertexIndexesEnable) {
				vertexIndexesEnable = 1;
			}
		}

		index = ((Face *) node->data)->vt;
		if (index > -1) {
			*((indexes->uv) + i) = index;
			if (!uvIndexesEnable) {
				uvIndexesEnable = 1;
			}
		} 
		index = ((Face *) node->data)->vn;
		if (index > -1) {
			*((indexes->normal) + i) = index;
			if (!normalIndexesEnable) {
				normalIndexesEnable = 1;
			}
		}
		++i;
	}
	listRewind(object->faceData);
	
	header->size = sizeof(B3Header);
	header->version = B3VERSION;

	header->vertexDataOffset = header->size;
	header->vertexDataSize = vertexDataSize;
	header->vertexIndexesEnable = vertexIndexesEnable;
	header->vertexIndexesDataOffset = header->vertexDataOffset + vertexDataSize;
	if (vertexIndexesEnable) {
		header->vertexIndexesDataSize = indexDataSize;
	} else {
		header->vertexIndexesDataSize = 0; 
	}

	header->uvDataOffset = header->vertexIndexesDataOffset + header->vertexIndexesDataSize;
	header->uvDataSize = uvDataSize;
	header->uvIndexesEnable = uvIndexesEnable;
	header->uvIndexesDataOffset = header->uvDataOffset + uvDataSize;
	if (uvIndexesEnable) {
		header->uvIndexesDataSize = indexDataSize;
	} else {
		header->uvIndexesDataSize = 0;
	}

	header->normalDataOffset = header->uvIndexesDataOffset + header->uvIndexesDataSize;
	header->normalDataSize = normalDataSize;
	header->normalIndexesEnable = normalIndexesEnable;
	header->normalIndexesDataOffset = header->normalDataOffset + normalDataSize;
	if (normalIndexesEnable) {
		header->normalIndexesDataSize = indexDataSize;
	} else {
		header->normalIndexesDataSize = 0;
	}

	return b3;
}

void b3Destroy(B3 *b3, int destroyDataDeep)
{
	if (destroyDataDeep) {
		free(b3->data->vertex);
		free(b3->data->uv);
		free(b3->data->normal);
		free(b3->indexes->vertex);
		free(b3->indexes->uv);
		free(b3->indexes->normal);
	}
	free(b3->header);
	free(b3->data);
	free(b3->indexes);
	free(b3);
}

int b3Save(char *fileName)
{
	/* @TODO */
	return 0;
}

