#ifndef _OBJECT_H_
#define _OBJECT_H_

#include "linked_list.h"

typedef struct {
    int v;
    int vt;
    int vn;
} Face;

typedef struct {
    float x;
    float y;
    float z;
    float w;
} Vertex;

typedef struct {
    float x;
    float y;
    float z;
} Normal;

typedef struct {
    float u;
    float v;
    float w;
} Uv;

typedef struct {
    TList *vertexData;
    TList *uvData;
    TList *normalData;
    TList *faceData;
} Object;

Object *objectCreate(void);
Object *objectParse(Object *, char *);
void objectDestroy(Object *);
void objectPurge(Object *, int, int);

#endif
