/* WARNING: this code is garbage for now :P */

#include "stdio.h"
#include "stdlib.h"
#include "string.h"
#include "linked_list.h"
#include "object.h"
#include "b3.h"

int main(int argc, char const **argv)
{
    int lineLength = 2048;
    char *in = "./test/ducky.obj",
	*line = malloc(sizeof(char) * lineLength);
    FILE *inFile;
    Object *object = objectCreate();
    B3 *b3;

    if ((inFile = fopen(in, "r")) == NULL) {
	printf("Could not open input file\n");
	return 1;
    }

    while ((line = fgets(line, lineLength, inFile)) != NULL) {
	objectParse(object, line);
    }

    b3 = b3CreateFromObj(object);
    if (!b3Save("./dump.b3")) {
        printf("There was an error while saving data\n");
    }
    
    printf("vertices: %d\n", object->vertexData->length);
    printf("uv data: %d\n", object->uvData->length);
    printf("normals: %d\n", object->normalData->length);
    printf("faces indices: %d\n", object->faceData->length);

    b3Destroy(b3, 1);
    objectPurge(object, 1, 1);
    free(line);

    /*if ((outFile = fopen(out, "w+")) == NULL) {
       printf("Could not open output file\n");
       return 1;
       } */

    return 0;
}
