#include "stdlib.h"
#include "linked_list.h"

TList *listCreate()
{
    TList *list = malloc(sizeof(TList));
    list->current = list->head = list->tail = NULL;
    list->length = 0;
    return list;
}

void listDestroy(TList * list)
{
    free(list);
}

TNode *listAddData(TList * list, void *data)
{
    TNode *node = malloc(sizeof(TNode));
    node->data = data;
    node->next = NULL;
    return listAdd(list, node);
}

TNode *listAdd(TList * list, TNode * node)
{
    if (list->head == NULL) {
	list->head = node;
    }

    if (list->current == NULL) {
	list->current = node;
    }

    if (list->tail != NULL) {
	list->tail->next = node;
    }

    list->tail = node;

    ++(list->length);
    return node;
}

int listRemoveByData(TList * list, void *data,
		     int (*compare) (const void *a, const void *b))
{
    TNode *current = list->head, *previous;
    previous = NULL;
    while (current != NULL) {
	if (compare(current->data, data) == 0) {
	    if (list->current == current) {
		list->current = current->next;
	    }

	    if (list->head == current) {
		list->head = current->next;
	    }

	    if (list->tail == current) {
		list->tail = current->next;
	    }

	    if (previous != NULL) {
		previous->next = current->next;
	    }

	    --(list->length);
	    return 1;
	}
	previous = current;
	current = current->next;
    }

    return 0;
}

int listRemove(TList * list, TNode * node,
	       int (*compare) (const TNode * a, const TNode * b))
{
    TNode *current = list->head, *previous;
    previous = NULL;
    while (current != NULL) {
	if (compare(current, node) == 0) {
	    if (list->current == current) {
		list->current = current->next;
	    }

	    if (list->head == current) {
		list->head = current->next;
	    }

	    if (list->tail == current) {
		list->tail = current->next;
	    }

	    if (previous != NULL) {
		previous->next = current->next;
	    }

	    --(list->length);
	    return 1;
	}
	previous = current;
	current = current->next;
    }

    return 0;
}

int listHasNextNode(TList * list)
{
    if (list->current != NULL && list->current->next != NULL) {
	return 1;
    }

    return 0;
}

TNode *listNextNode(TList * list)
{
    TNode *node = list->current;
    if (node != NULL) {
	list->current = node->next;
    }
    return node;
}

void listRewind(TList * list)
{
    list->current = list->head;
}

void listPurge(TList * list, int destroyData, int destroyList)
{
    TNode *node = list->head, *next;
    list->current = NULL;
    list->head = NULL;
    list->tail = NULL;

    while (node) {
	next = node->next;
	if (destroyData) {
	    free(node->data);
	}
	free(node);
	node = next;
    }

    if (destroyList) {
	listDestroy(list);
    }
}
