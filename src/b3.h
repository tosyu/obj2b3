#ifndef _B_3_H_
#define _B_3_H_

#ifndef B3VERSION
#define B3VERSION 1
#endif

#include "object.h"
#include "linked_list.h"

typedef struct {
	int size;
	int version;
	int vertexDataOffset;
	int vertexDataSize;
	int vertexIndexesEnable;
	int vertexIndexesDataOffset;
	int vertexIndexesDataSize;
	int uvDataOffset;
	int uvDataSize;
	int uvIndexesEnable;
	int uvIndexesDataOffset;
	int uvIndexesDataSize;
	int normalDataOffset;
	int normalDataSize;
	int normalIndexesEnable;
	int normalIndexesDataOffset;
	int normalIndexesDataSize;
} B3Header;

typedef struct {
	float *vertex;
	float *uv;
	float *normal;
} B3Data;

typedef struct {
	int *vertex;
	int *uv;
	int *normal;
} B3Indexes;

typedef struct {
	B3Header *header;
	B3Data *data;
	B3Indexes *indexes;
} B3;

B3 *b3Create(void);
B3 *b3CreateFromObj(Object *);
void b3Destroy(B3 *, int);
int b3Save(char *);

#endif
